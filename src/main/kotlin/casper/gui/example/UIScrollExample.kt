package casper.gui.example

import casper.UICustomFactory
import casper.gui.UINode
import casper.gui.UIScene
import casper.gui.component.UIImage
import casper.gui.component.scroll.UIScrollBox
import casper.gui.component.text.UIText
import casper.gui.layout.Layout
import casper.types.ScaleTexture
import casper.types.Texture

class UIScrollExample(uiScene: UIScene) {
	val tab = UICustomFactory.createTab(uiScene, "scroll demo")

	init {
		val node = tab.content
		node.layout = Layout.VERTICAL

		val txtInfo = UIText.create(uiScene, "")
		txtInfo.node.setSize(200, 60)
		node += txtInfo.node
//
//		val scrollVertical = UIScroll.create(uiFactory, style.scrollStyle, true)
//		scrollVertical.node.setSize(30, 90)
//		node += scrollVertical.node
//
//		scrollVertical.onValue.then {
//			txtInfo.text = "scroll (V):\n ${scrollVertical.onValue.get()}"
//		}
//
//		val scrollHorizontal = UIScroll.create(uiFactory, style.scrollStyle, false)
//		scrollHorizontal.node.setSize(90, 30)
//		node += scrollHorizontal.node
//
//		scrollHorizontal.onValue.then {
//			txtInfo.text = "scroll (H):\n ${scrollHorizontal.onValue.get()}"
//		}
//
//		val boxA = UIBox.createExternal(uiFactory, style.boxStyle)
//		boxA.node.setSize(200, 150)
//		val contentA = createContent(uiFactory, 500, 600)
//		boxA.content.add(contentA)
//		boxA.content.setSize(contentA.getSize())
//
//		boxA.node.update<UIMoveByPointer> { mover ->
//			mover.onMove.then { position ->
//				txtInfo.text = "external:\n ${position.x}; ${position.y}\nposition: ${boxA.content.getPosition().x}; ${boxA.content.getPosition().y}"
//			}
//		}
//
//		node += boxA.node

		node += createScrollBox(uiScene, txtInfo,  500, 600).node
		node += createScrollBox(uiScene, txtInfo,  250, 600).node
		node += createScrollBox(uiScene, txtInfo,  550, 100).node
		node += createScrollBox(uiScene, txtInfo,  250, 100).node
	}

	private fun createScrollBox(uiScene: UIScene, txtInfo: UIText,  width: Int, height: Int): UIScrollBox {
		val scrollBox = UIScrollBox.create(uiScene)
		setupContent(scrollBox.box.content, width, height)

		scrollBox.node.setSize(500, 150)
		scrollBox.onMove.then {
			txtInfo.text = "scroll box request:\n$it"
		}
		return scrollBox
	}

	private fun setupContent(content:UINode, width: Int, height: Int) {
		UIImage(content, ScaleTexture(Texture("paper.png"), 0.0))
		content.setSize(width, height)
	}
}