package casper.gui.example

import casper.UICustomFactory
import casper.collection.ObservableMutableList
import casper.collection.observableListOf
import casper.gui.UIScene
import casper.gui.component.button.UIButton
import casper.gui.component.button.UIButtonWithLabel
import casper.gui.component.panel.UIPanel
import casper.gui.component.tab.TabContentAlign
import casper.gui.component.tab.TabMenuStyle
import casper.gui.component.tab.UITab
import casper.gui.component.tab.UITabMenu
import casper.gui.component.text.UIText
import casper.gui.component.toggle.UIToggle
import casper.gui.component.toggle.UIToggleWithLabel
import casper.gui.layout.Align
import casper.gui.layout.Direction
import casper.gui.layout.Layout
import casper.gui.util.BooleanGroupRule
import casper.signal.util.then


class UITabExample(val uiScene: UIScene) {
	val tab = UICustomFactory.createTab(uiScene, "tabs")
	var tabs: ObservableMutableList<UITab>
	val node = tab.content
	val txtInfo = UIText.create(uiScene, "")
	var tabMenu: UITabMenu? = null

	init {
		node.layout = Layout.VERTICAL

		txtInfo.node.setSize(200, 50)
		node += txtInfo.node
		tabs = createTabs()

		node += UIButton.createWithText(uiScene, "add tab") {
			tabs.add(UITab(
					UIToggle.createWithText(uiScene, "gen btn ${tabs.size + 1}").node.setSize(90, 40),
					UIText.create(uiScene, "generated\ncontent").node
			))
			txtInfo.text = "Tab added"
		}.node.setSize(120, 30)

		node += UIButton.createWithText(uiScene, "remove tab") {
			tabs.lastOrNull()?.let {
				tabs.remove(it)
				txtInfo.text = "Tab removed"
			}
		}.node.setSize(120, 30)

		node += UIButton.createWithText(uiScene, "Create left\ntab menu") {
			createTabMenu(Direction.LEFT)
		}.node.setSize(160, 40)


		node += UIButton.createWithText(uiScene, "Create right\ntab menu") {
			createTabMenu(Direction.RIGHT)
		}.node.setSize(160, 40)

		node += UIButton.createWithText(uiScene, "Create top\ntab menu") {
			createTabMenu(Direction.TOP)
		}.node.setSize(160, 40)


		node += UIButton.createWithText(uiScene, "Create bottom\ntab menu") {
			createTabMenu(Direction.BOTTOM)
		}.node.setSize(160, 40)


		createTabMenu(Direction.LEFT)

		node.onScene.then {
			println("tab visible: $it")
		}
	}

	private fun createTabs(): ObservableMutableList<UITab> {
		val btn1 = UIToggle.createWithText(uiScene, "btn1").node.setSize(70, 40)
		val btn2 = UIToggle.createWithText(uiScene, "btn2").node.setSize(70, 40)
		val btn3 = UIToggle.createWithText(uiScene, "btn3").node.setSize(70, 40)
		val btn4 = UIToggle.createWithText(uiScene, "btn4").node.setSize(70, 40)
		val content1 = UIPanel(UIButton.createWithText(uiScene, "content\n320x40").node).node.setSize(320, 40)
		val content2 = UIPanel(UIButton.createWithText(uiScene, "content\n160x80").node).node.setSize(160, 80)
		val content3 = UIPanel(UIButton.createWithText(uiScene, "content\n80x160").node).node.setSize(80, 160)
		val content4 = UIPanel(UIButton.createWithText(uiScene, "content\n40x320").node).node.setSize(40, 320)
		return observableListOf(UITab(btn1, content1), UITab(btn2, content2), UITab(btn3, content3), UITab(btn4, content4))
	}

	fun createTabMenu(direction: Direction) {
		tabMenu?.node?.dispose()
		tabs = createTabs()

		val nextMenu = UITabMenu.create(uiScene, direction, TabContentAlign(false, Align.CENTER, Align.CENTER), BooleanGroupRule.MAX_ONE_TRUE, tabs)
		UIPanel(nextMenu.node)

		nextMenu.node.setSize(200, 200)
		nextMenu.onTabSelected.then { tab ->
			if (tabs.contains(tab)) {
				val tabId = (1 + tabs.indexOf(tab))
				txtInfo.text = "Tab $tabId selected"
			} else {
				txtInfo.text = "Invalid tab"
			}
		}

		tabMenu = nextMenu
		node += nextMenu.node
		txtInfo.text = "Tab menu created"
	}
}